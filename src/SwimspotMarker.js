import React from "react";


class SwimspotMarker extends React.Component {
	render() {
		const airTemp = this.props.airTemp
		const waterTemp = this.props.waterTemp
		const timeUpdated = this.props.timeUpdated
		const airTemperatureToggled = this.props.airTemperatureToggled
		// check whether temperatures have been updated in the last hour
		let fillColor = Math.round(((new Date() - Date.parse(timeUpdated)) /1000/60)) <= 60 ? "yellow" : "red"
		const marker = <circle className="marker" cx="21" cy="21" r="17" fill = {fillColor}></circle>


		return (
			
			<svg width="60px" height="60px" viewBox="0 0 42 42" className="markerImg" role="img">
				{marker}
				
				<g className="chart-text">
					<text className="chart-number" x="15%" y="60%">
					{airTemperatureToggled ? airTemp : waterTemp}
					</text>
				</g>
			</svg>
		);
	}
}
export default SwimspotMarker
import React from "react"
import { MapContainer as LeafletMap , Marker, Popup, TileLayer, Polyline} from "react-leaflet"
import getTemps from "./getTemps"
import {DivIcon} from "leaflet"
import ReactDOMServer from "react-dom/server"
import SwimspotMarker from "./SwimspotMarker"

class Map extends React.Component {

	constructor() {
		super()
		this.state = {
			swimmingSpots: [],
			airTemperatureToggled: true
		}
	}
componentDidMount() {

	getTemps.getMetadata().then(response => {
		let spots = []
		let ids = Object.keys(response.data) // Get id for each swimming spot

		let swimmingSpotPromises = ids.map(
			id => {return(
				getTemps.getData(id).then(spot => {
					let lastMeasurement = spot.data.data.slice(-1)[0]
					let swimmingspot = {...spot.data.meta, ...lastMeasurement} // Add metadata and measurement data to each swimmingspot object
					return(swimmingspot)
				})
			)}
		)
		Promise.all(swimmingSpotPromises).then(swimmingSpots => this.setState({swimmingSpots}))
		
	})
}

toggleTemperatures =(e) => {
	e.preventDefault()
	this.setState(previousState => ({
		airTemperatureToggled: !previousState.airTemperatureToggled
	  }))
}

render() {


	const swimmingSpotMarkers = this.state.swimmingSpots.map((spot,i) => {
		let icon = new DivIcon({
			className: "custom-icon",
			html: ReactDOMServer.renderToString(<SwimspotMarker  airTemp={spot.temp_air} waterTemp={spot.temp_water} timeUpdated={spot.time} airTemperatureToggled={this.state.airTemperatureToggled}/>)
		})
		return (
			<Marker  key ={i} position={[spot.lat,spot.lon]} icon = {icon}>
				<Popup key = {i}>
							Name: {spot.name} <br></br>
							Water temperature: {spot.temp_water}  <br></br>
							Air temperature: {spot.temp_air} <br></br>
							Updated: {spot.time} <br></br>
							{Math.round((new Date() - Date.parse(spot.time)) /1000/60)} minutes ago
						</Popup>
			</Marker>
		)
	})

	const locationButtonStyle = {
		position:"absolute",
		backgroundColor: "#b3b3ff",
		top: "15%", left:0, border:"2px solid black",
		borderRadius: "50%",
		padding: "1%",
		zIndex: 10000,
	}

	return(
		<div>
		<LeafletMap center={[60.21,24.94]} zoom={11} scrollWheelZoom={true}>
		<TileLayer
		  attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
		  url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
		/>
		{this.state.swimmingSpots.length >= 1 ? swimmingSpotMarkers : null}
		<button style = {locationButtonStyle} onClick={e => this.toggleTemperatures(e)}> {this.state.airTemperatureToggled ? "Show water temperatures" : "Show air temperatures"}</button>
	  </LeafletMap>
	  </div>
	)

}


}

export default Map
import axios from "axios"

const baseUrl = "http://iot.fvh.fi/opendata/uiras/"

const getMetadata = async () => {
	const request = await axios.get(baseUrl+"uiras-meta.json")
	return request
}
const getData = async(id) => {
	const request = await axios.get(`${baseUrl}${id}_v1.json`)
	return request
}

export default {getMetadata,getData}